import sys
import os
import io
import argparse
import html
import json
import jmespath
from bs4 import BeautifulSoup, Tag, Doctype
from book import EPub, Chapter, BookItem, DataFromPath

def strip_remote_refs(soup, src_map):
	modified = False
	for el in soup.find_all('img') + soup.find_all('script'):
		try:
			bi = src_map.get(el['src'], None)
			if bi is None:
				for prefix in ('http:/', 'https:/'):
					if el['src'][:len(prefix)] == prefix:
						del el['src']
						modified = True
						break
			else:
				el['src'] = bi.id
				modified = True
		except KeyError:
			pass
	return modified

def __run(items, file_out
	, exp_chapter_text, exp_chapter_title, exp_chapter_uid
	, uid, scheme_uid, title, author, description, source, date
	, cover, references, strip_remote, verbose
):
	src_map = {}
	refs_already = {}
	refs = []
	for r in references:
		try:
			l = json.loads(r)
		except ValueError:
			l = None
		if l is None:
			refitems = [r.split('#', 3)]
		else:
			if isinstance(l, dict):
				l = [l]
			refitems = [(((m['url'],) if (m.get('file', None) is None) else (m['url'], m['file']))
					if (m.get('mime', None) is None) else (m['url'], m['file'], m['mime']))
				for m in l]
		for parts in refitems:
			if len(parts) > 1:
				if len(parts) > 2:
					url, filename, mimetype = parts
				else:
					url, filename = parts
					mimetype = 'image/jpeg'
				bi = refs_already.get((filename, mimetype), None)
				if bi is None:
					bi = BookItem(('r' + str(len(refs))), DataFromPath(filename), mimetype)
					refs_already[(filename, mimetype)] = bi
					refs.append(bi)
				src_map[url] = bi
			else:
				url = parts[0]
				# Download URL and then add mapping

	chapters = []
	for i, item in enumerate(items):
		chap = str(i + 1)
		filename = (chap + '.htm')

		text_chapter = jmespath.search(exp_chapter_text, item)
		data_chapter = text_chapter if isinstance(text_chapter, str) else ''.join(item['vals'])
		if not data_chapter:
			continue
		soup = BeautifulSoup(data_chapter, features='html.parser')
		if soup.body is None:
			soup.body = Tag(name='body')
			for el in list(soup.children):
				soup.body.append(el)
			soup.append(soup.body)
		if soup.html is None:
			soup.html = Tag(name='html')
			soup.html.append(soup.body)
			soup.append(Doctype('html'))
			soup.append(soup.html)

		title_chapter = jmespath.search(exp_chapter_title, item) if exp_chapter_title else None
		if soup.html.head is None:
			soup.html.head = Tag(name='head')
			soup.html.insert(0, soup.html.head)
		if soup.html.head.title is None:
			title_parts = []
			if title:
				title_parts.append(title)
			if title_chapter:
				title_parts.append(title_chapter)
			if title_parts:
				soup.title = Tag(name='title')
				soup.title.string = ': '.join(title_parts)
				soup.html.head.append(soup.title)

		if strip_remote:
			modified = strip_remote_refs(soup, src_map)

		uid_chapter = jmespath.search(exp_chapter_uid, item) if exp_chapter_uid else None
		if title_chapter:
			el_header = Tag(name='h2')
			el_title = el_header
			if uid_chapter and (scheme_uid == 'URI'):
				el_title = Tag(name='a')
				el_title['href'] = uid_chapter
				el_header.append(el_title)
			el_title.string = title_chapter
			soup.body.insert(0, el_header)
		elif uid_chapter:
			el_footer = Tag(name='div')
			el_footer['class'] = 'uid footer'
			if scheme_uid == 'URI':
				el_uid = Tag(name='a')
				el_uid['href'] = uid_chapter
			else:
				el_uid = Tag(name='span')
			el_uid.string = uid_chapter
			el_footer.append(el_uid)
			soup.body.append(el_footer)
		chapters.append(Chapter(str(soup), title_chapter))

	book = EPub(chapters, refs)
	if cover:
		parts = cover.split('#', 2)
		if len(parts) > 1:
			filename, mimetype = cover
		else:
			filename = cover
			mimetype = 'image/jpeg'
		book.cover = refs_already.get((filename, mimetype), None)
		if book.cover is None:
			book.cover = BookItem('cover', DataFromPath(filename), mimetype)

	book.title = title
	book.author = author
	book.source = source
	book.uid = uid
	book.scheme_uid = scheme_uid
	book.description = description
	book.date = date
	book.save(file_out)
	return book

def __main():
	parser = argparse.ArgumentParser(description="""Gather information from a series of pages/URLs""")
	parser.add_argument(dest='exp_text', default='vals', nargs='?'
		, help="""Get Chapter text from JMESPath EXPRESSION (also known as JSONPath, evaluate against each individual item, default to "vals")""", metavar='EXPRESSION')
	parser.add_argument('-i', '--infile', dest='infile', help="""Read input JSON from FILE (default to STDIN)""", metavar='FILE')
	parser.add_argument('-o', '--outfile', dest='outfile', help="""Output to EPUB FILE (default to STDOUT)""", metavar='FILE')
	parser.add_argument('-t', '--title', dest='title', help="""Set book title to TITLE""", metavar='TITLE')
	parser.add_argument('-a', '--author', dest='author', help="""Set book author to AUTHOR""", metavar='AUTHOR')
	parser.add_argument('-u', '--uid', dest='uid', help="""Set unique ID for book to UID""", metavar='UID')
	parser.add_argument('--uid-scheme', dest='scheme_uid', help="""SCHEME for unique ID (e.g. ISBN, URI)""", metavar='SCHEME')
	parser.add_argument('-e', '--desc', '--description', dest='description', help="""Set book description to DESCRIPTION""", metavar='DESCRIPTION')
	parser.add_argument('-s', '--source', dest='source'
		, help="""Set book source to SOURCE (default to unique ID if ID scheme is URI)""", metavar='SOURCE')
	parser.add_argument('-d', '--date', dest='date', help="""Set book timestamp to DATE (default to now)""", metavar='DATE')
	parser.add_argument('-c', '--chapter-title', dest='exp_title', help="""Add Chapter title from JMESPath EXPRESSION""", metavar='EXPRESSION')
	parser.add_argument('--chapter-uid', dest='exp_uid', help="""Associate UID with Chapter from JMESPath EXPRESSION""", metavar='EXPRESSION')
	parser.add_argument('--cover', dest='cover', help="""Set book cover to FILE or URL""", metavar='FILE')
	parser.add_argument('-r', '--reference', dest='references', action='append', help="""Download item at URL and add it to book as a local resource""", metavar='URL')
	parser.add_argument('-x', '--strip-remote', dest='strip_remote', action='store_true'
		, help="""Strip remote references in elements (e.g. Images, Scripts)""")
	parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help="""Output more messages""")
	args = parser.parse_args()

	with (sys.stdin if args.infile is None else open(args.infile, 'r')) as f_in:
		items = json.loads(f_in.read())
	return __run(items, (sys.stdout.buffer if args.outfile is None else args.outfile)
		, args.exp_text, args.exp_title, args.exp_uid
		, args.uid, args.scheme_uid, args.title, args.author, args.description, args.source, args.date
		, args.cover, args.references or (), args.strip_remote, args.verbose)

if __name__ == '__main__':
	book = __main()


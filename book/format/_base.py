import io

class Book(object):
	cover = None
	title = None
	author = None
	chapters = ()
	def save(self, f_out):
		raise NotImplementedError
	def load(self, f_in):
		raise NotImplementedError

class Chapter(object):
	def __init__(self, content, title = None):
		self.title = title
		self._content = content
	@property
	def content(self): return self._content

class _DataSource(object):
	def get_data(self):
		return self.process_data(lambda f: f.read())
	def process_data(self, handle):
		raise NotImplementedError
class DataFromPath(_DataSource):
	def __init__(self, path):
		self._path = path
	def process_data(self, handle):
		with open(self._path, 'rb') as f:
			return handle(f)
class DataFromBytes(_DataSource):
	def __init__(self, data):
		self._data = data
	def process_data(self, handle):
		with io.BytesIO(self._data) as f:
			return handle(f)
class DataFromFile(_DataSource):
	def __init__(self, f):
		self._f = f
	def process_data(self, handle):
		return handle(self._f)
class DataFromFunction(DataFromBytes):
	def __init__(self, fx, *args, **kwargs):
		self._fx = fx
		self._args = (args, kwargs)
	@property
	def _data(self):
		args, kwargs = self._args
		return self._fx(*args, **kwargs)

class BookItem(object):
	def __init__(self, id, data_source = None, mimetype = None):
		self.id = id
		self.mimetype = mimetype
		self.data = (data_source or DataFromBytes(b''))
	def __hash__(self): return hash(self.id)


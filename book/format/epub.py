import io
import argparse
import zipfile
import datetime
import html
import urllib.parse
from lxml import etree
from ._base import Book as BookBase, BookItem as BookItemBase, Chapter as ChapterBase, DataFromFunction

NS_XML = 'http://www.w3.org/XML/1998/namespace'

def extract_xmlns(el):
	if el.tag[:1] == '{':
		pos_end = el.tag.find('}')
		if pos_end > 0:
			return el.tag[1:pos_end], el.tag[pos_end + 1:]
	return '', el.tag
def xfind(el, path):
	return el.find(path, el.nsmap)

def expand_ns(ns, key, nsmap):
	return etree.QName(nsmap[ns], key)

def list_tree_transfer(nodes_root, get_children, node_to_item, add_child, item_root = None):
	if add_child is None:
		def add_child(*args): pass # Do nothing; perhaps the node_to_item function already handles the attachment
	items_root = None
	if item_root is None:
		add_child_nonroot = add_child
		items_root = []
		item_root = object() # Create special single object for checking whether to invoke addition to managed list
		def add_child(parent, item):
			if parent == item_root:
				items_root.append(item)
			else:
				return add_child_nonroot(parent, item)
	nodes = [(node, node_to_item(node, item_root), item_root) for node in nodes_root]
	while nodes:
		node, item, item_parent = nodes.pop(0) # Remove from front
		add_child(item_parent, item)
		for node_child in get_children(node):
			item_child = node_to_item(node_child, item_parent)
			nodes.append((node_child, item_child, item))
	return items_root

class BookItem(BookItemBase):
	def __init__(self, id, href, data_source, mimetype, title, items_sub = None):
		super().__init__(id, data_source, mimetype)
		self.href = href
		self.title = title
		self.items = ([] if (items_sub is None) else items_sub)
class Chapter(ChapterBase):
	def __init__(self, content, title, href, mimetype):
		super().__init__(content, title)
		self.path, self.fragment = urllib.parse.urldefrag(href)
		self.mimetype = mimetype
		self.sub = []
	@property
	def href(self): return ((self.path + '#' + self.fragment) if self.fragment else self.path)
class Resource(BookItemBase):
	def __init__(self, content, id, path, mimetype):
		self._content = content
		super().__init__(id, DataFromFunction(lambda: self.content), mimetype)
		self.path = path
	@property
	def content(self): return self._content

class XMLFragment(object):
	DOCTYPE = None
	nsmap = {}
	tag_root = None
	def to_xml(self, *args, **kwargs):
		raise NotImplementedError
	def as_bytes(self, *args, **kwargs):
		params = {'encoding': 'utf-8', 'xml_declaration': True}
		if self.DOCTYPE is not None:
			params['doctype'] = self.DOCTYPE
		return etree.tostring(self.to_xml(*args, **kwargs), **params)
	@classmethod
	def from_el(cls, el, *args, **kwargs): return cls._from_el(cls.check_node(el, cls.tag_root), *args, **kwargs)
	@classmethod
	def _from_el(cls, el):
		raise NotImplementedError
	@classmethod
	def check_node(cls, el, tag, nsmap = None):
		if el.tag != expand_ns(None, tag, (nsmap or cls.nsmap)):
			raise InvalidDataError('Incorrect Tag for Root node: {} (expected {})'.format(el.tag, tag))
		return el

class Container(XMLFragment):
	nsmap = {None: 'urn:oasis:names:tc:opendocument:xmlns:container'}
	tag_root = 'container'
	def __init__(self, path_opf):
		self.path_opf = path_opf
	def to_xml(self):
		el = etree.Element(self.tag_root, version='1.0', nsmap=self.nsmap)
		etree.SubElement(etree.SubElement(el, 'rootfiles'), 'rootfile'
			, attrib={'media-type': BookContent.MIMETYPE, 'full-path': self.path_opf})
		return el
	@classmethod
	def _from_el(cls, el):
		elContent = el.find("rootfiles/rootfile[@media-type='{}']".format(BookContent.MIMETYPE)
			, cls.nsmap)
		return (None if (elContent is None) else cls(elContent.attrib.get('full-path')))

	def url_for_item(self, bi):
		url = urllib.parse.urljoin(self.path_opf, bi.href)
		return (url, urllib.parse.urldefrag(url)[0])

class BookMetadata(XMLFragment):
	nsmap = {
		'dc': 'http://purl.org/dc/elements/1.1/',
		'xsi':'http://www.w3.org/2001/XMLSchema-instance',
		'opf':'http://www.idpf.org/2007/opf',
		'dcterms':'http://purl.org/dc/terms/'}
	def __init__(self, id, scheme_id, key_id, **kwargs):
		self.id = id
		self.scheme_id = scheme_id
		self.key_id = key_id
		self.fields = kwargs
	def set(self, key, val):
		self.fields[key] = val
	def get(self, key, default):
		return self.fields.get(key, default)
	def to_xml(self):
		el = etree.Element('metadata', nsmap=self.nsmap)
		field_attrs = {'creator': (lambda k, v: {expand_ns('opf', 'file-as', el.nsmap): v, expand_ns('opf', 'role', el.nsmap): 'aut'})}
		for key, val in self.fields.items():
			if val is not None:
				get_attrs = field_attrs.get(key, None)
				el_meta = etree.SubElement(el, expand_ns('dc', key, el.nsmap)
					, attrib=(get_attrs(key, val) if get_attrs else None))
				el_meta.text = val

		cover = self.fields.get('cover', None)
		if cover:
			etree.SubElement(el, expand_ns('opf', 'meta', el.nsmap), content='cover', name=cover)
		if self.id:
			el_id = etree.SubElement(el, expand_ns('dc', 'identifier', el.nsmap), id=self.key_id)
			if self.scheme_id:
				el_id.attrib[expand_ns('opf', 'scheme', el.nsmap)] = self.scheme_id
			el_id.text = self.id
		return el
	@classmethod
	def from_el(cls, el, key_id):
		cls.check_node(el, 'metadata', BookContent.nsmap)
		fields = {}
		el_cover = el.find("opf:meta[@content='cover']", cls.nsmap)
		if el_cover is not None:
			fields['cover'] = el_cover.attrib.get('name')
		el_author = el.find("dc:creator[@opf:role='aut']", cls.nsmap)
		if el_author is not None:
			fields['creator'] = el_author.text
		for el_node in el.iter(tag = etree.Element):
			qn = etree.QName(el_node.tag)
			if (qn.namespace == cls.nsmap['dc']) and (qn.localname not in fields):
				fields[qn.localname] = el_node.text
		el_id = el.find("dc:identifier[@id='{}']".format(key_id), cls.nsmap)
		if el_id is None:
			el_id = etree.Element('none')
		return cls(el_id.text, el_id.attrib.get(expand_ns('opf', 'scheme', cls.nsmap)), key_id, **fields)

class BookManifest(XMLFragment):
	def __init__(self, items = None):
		self.items = ([] if (items is None) else items)
	def to_xml(self):
		el = etree.Element('manifest')
		for item in self.items:
			etree.SubElement(el, 'item', attrib={'id': item.id, 'href': item.href, 'media-type': item.mimetype})
		return el
	@classmethod
	def from_el(cls, el):
		cls.check_node(el, 'manifest', BookContent.nsmap)
		return cls([BookItem(
			el_item.attrib.get('id'),
			el_item.attrib.get('href'),
			None,
			el_item.attrib.get('media-type'),
			None
		) for el_item in el.findall('item', BookContent.nsmap)])

class BookContent(XMLFragment):
	MIMETYPE = 'application/oebps-package+xml'
	MIMETYPE_TEXT = 'application/xhtml+xml'
	KEY_ID = 'BookId'
	tag_root = 'package'
	nsmap = {None: 'http://www.idpf.org/2007/opf'}
	def __init__(self, id, scheme_id = None, metadata = None, manifest = None):
		self.metadata = metadata or BookMetadata(id, scheme_id, self.KEY_ID)
		self.manifest = manifest or BookManifest()
		self.nav_control = None
		self.spine = []
	def to_xml(self, id, scheme_id):
		el = etree.Element(self.tag_root, attrib={'version': '2.0'}, nsmap=self.nsmap)
		el.append(self.metadata.to_xml())
		el.append(self.manifest.to_xml())
		el_spine = etree.SubElement(el, 'spine')
		if self.nav_control is not None:
			el_spine.attrib['toc'] = self.nav_control.id
		for item in self.spine:
			etree.SubElement(el_spine, 'itemref', idref=item.id)
		#etree.SubElement(el, 'guide')
		return el
	@classmethod
	def _from_el(cls, el):
		metadata = BookMetadata.from_el(el.find('metadata', cls.nsmap), cls.KEY_ID)
		if metadata is None:
			raise InvalidDataError('Content contains no Metadata')
		manifest = BookManifest.from_el(el.find('manifest', cls.nsmap))
		if manifest is None:
			raise InvalidDataError('Content contains no Manifest')
		map_items = dict((bi.id, bi) for bi in manifest.items)
		content = cls(None, None, metadata, manifest)
		el_spine = el.find('spine', cls.nsmap)
		if el_spine is not None:
			content.nav_control = map_items.get(el_spine.attrib.get('toc'), None)
			for el_item in el_spine.findall('itemref', cls.nsmap):
				bi = map_items.get(el_item.attrib.get('idref'), None)
				if bi is not None:
					content.spine.append(bi)
		return content

class NavigationControl(XMLFragment):
	MIMETYPE = 'application/x-dtbncx+xml'
	DOCTYPE = '<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN" "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">'
	tag_root = 'ncx'
	nsmap = {None: 'http://www.daisy.org/z3986/2005/ncx/'}
	def __init__(self, uid, id, title, author):
		self.uid = uid
		self.id = id
		self.title = title
		self.author = author
		self.items = []
	def to_xml(self):
		el = etree.Element('ncx', attrib={'version': '2005-1'}, nsmap=self.nsmap)
		el.set(etree.QName(NS_XML, 'lang'), 'en')
		el_head = etree.SubElement(el, 'head')

		el_title = etree.SubElement(etree.SubElement(el, 'docTitle'), 'text')
		el_title.text = self.title
		el_author = etree.SubElement(etree.SubElement(el, 'docAuthor'), 'text')
		el_author.text = self.author
		el_map = etree.SubElement(el, 'navMap')
		order = 1
		depth_max = 1
		def item_to_el(item, el_parent):
			el_nav = etree.SubElement(el_parent, 'navPoint', id=item.id, playOrder=str(order))
			el_label = etree.SubElement(etree.SubElement(el_nav, 'navLabel'), 'text')
			el_label.text = item.title
			etree.SubElement(el_nav, 'content', src=item.href)
			depth = 1
			while el_parent != el_map:
				depth += 1
				el_parent = el_parent.getparent()
			nonlocal depth_max
			depth_max = max(depth, depth_max)
		list_tree_transfer(self.items, (lambda item: item.items), item_to_el, None, el_map)
		etree.SubElement(el_head, 'meta', name='dtb:uid', content=('' if (self.uid is None) else self.uid))
		etree.SubElement(el_head, 'meta', name='dtb:depth', content=str(depth_max))
		etree.SubElement(el_head, 'meta', name='dtb:totalPageCount', content='0')
		etree.SubElement(el_head, 'meta', name='dtb:maxPageNumber', content='0')
		return el

	@classmethod
	def _from_el(cls, el, id):
		el_uid = el.find("head/meta[@name='dtb.uid']", cls.nsmap)
		uid = (None if (el_uid is None) else el_uid.attrib.get('content'))
		el_title = el.find('docTitle/text', cls.nsmap)
		title = (None if (el_title is None) else el_title.text)
		el_author = el.find('docAuthor/text', cls.nsmap)
		author = (None if (el_author is None) else el_author.text)
		el_map = el.find('navMap', cls.nsmap)
		nav = cls(uid, id, title, author)

		def el_to_bi(el_nav, item):
			el_content = el_nav.find('content', cls.nsmap)
			el_label = el_nav.find('navLabel/text', cls.nsmap)
			return BookItem(el_nav.attrib.get('id')
				, (None if (el_content is None) else el_content.attrib.get('src'))
				, None
				, None
				, (None if (el_label is None) else el_label.text))
		nav.items = list_tree_transfer([el_map]
			, (lambda el_parent: el_parent.findall('navPoint', cls.nsmap))
			, el_to_bi
			, (lambda parent, item: parent.items.append(item))
		)[0].items
		return nav

class InvalidDataError(ValueError):
	pass
class Book(BookBase):
	uid = None
	scheme_uid = None
	source = None
	description = None
	date = None
	mimetype = b'application/epub+zip'
	PATH_MIMETYPE = 'mimetype'
	PATH_CONTAINER = 'META-INF/container.xml'
	Chapter = Chapter
	Reference = Resource
	def __init__(self, chapters = None, references = None):
		self.chapters = ([] if (chapters is None) else chapters)
		self.references = ([] if (references is None) else references)
	def _open_epub(self, file_in):
		try:
			z_epub = zipfile.ZipFile(file_in, 'r')
		except zipfile.BadZipFile:
			raise InvalidDataError('File does not contain EPub data') from None
		if z_epub.read(self.PATH_MIMETYPE) != self.mimetype:
			with z_epub:
				raise InvalidDataError('File missing EPub MIME Type entry')
		return z_epub
	def _parse_xml_file(self, z, path):
		try:
			with io.BytesIO(z.read(path)) as f:
				return etree.parse(f).getroot()
		except etree.ParseError as ex:
			raise InvalidDataError('EPub file contains invalid data: {}: {}'.format(path, str(ex))) from None
	def load(self, file_in):
		with self._open_epub(file_in) as z_epub:
			return self._load(z_epub)
	def _load(self, z_epub):
		container = Container.from_el(self._parse_xml_file(z_epub, self.PATH_CONTAINER))
		if (container is None) or (container.path_opf is None):
			raise InvalidDataError('Container file does not indicate contents')
		content = BookContent.from_el(self._parse_xml_file(z_epub, container.path_opf))
		self.title = content.metadata.get('title', None)
		self.author = content.metadata.get('creator', None)
		self.uid = content.metadata.id
		self.scheme_uid = content.metadata.scheme_id
		self.description = content.metadata.get('description', None)
		self.source = content.metadata.get('source', None)
		self.date = content.metadata.get('date', None)
		id_cover = content.metadata.get('cover', None)

		ncx = None
		if content.nav_control is not None:
			href, path = container.url_for_item(content.nav_control)
			try:
				ncx = NavigationControl.from_el(self._parse_xml_file(z_epub, path), content.nav_control.id)
			except InvalidDataError:
				ncx = None
		if ncx is None:
			for bi in content.spine:
				href, path_content = container.url_for_item(bi)
				chapter = self.Chapter(self._get_content(z_epub, path_content), bi.title, href, bi.mimetype)
				self.chapters.append(chapter)
			items_main = set(content.spine)
		else:
			items_main = set()
			def chapter_create(bi, chapter_parent):
				items_main.add(bi)
				href, path_content = container.url_for_item(bi)
				return self.Chapter(self._get_content(z_epub, path_content), bi.title, href, bi.mimetype)
			self.chapters += list_tree_transfer(ncx.items
				, (lambda bi: bi.items)
				, chapter_create
				, (lambda parent, bi: parent.sub.append(bi)))
			items_main = set(ncx.items)

		for bi in (bi for bi in content.manifest.items if (bi not in items_main)):
			href, path_content = container.url_for_item(bi)
			ref = self.Reference(self._get_content(z_epub, path_content), bi.id, href, bi.mimetype)
			if id_cover == bi.id:
				self.cover = ref
			self.references.append(ref)
		return content
	def _get_content(self, z_epub, path): return z_epub.read(path)

	def save(self, file_out):
		opf = BookContent(self.uid, self.scheme_uid)
		ncx = NavigationControl(self.uid, 'ncx', self.title, self.author)
		opf.nav_control = BookItem(ncx.id, 'toc.ncx', DataFromFunction(ncx.as_bytes), ncx.MIMETYPE, '', False)
		opf.manifest.items.append(opf.nav_control)
		files = [(('OEBPS/' + opf.nav_control.href), opf.nav_control)]

		for i, item in enumerate(self.chapters):
			chap = str(i + 1)
			filename = (chap + '.xhtml')
			data = DataFromFunction(item.content.encode, 'utf-8')
			bi = BookItem(('chapter' + chap), filename, data, opf.MIMETYPE_TEXT
				, (('Chapter ' + chap) if (item.title is None) else item.title))
			files.append((('OEBPS/' + filename), bi))
			opf.manifest.items.append(bi)
			opf.spine.append(bi)
			ncx.items.append(bi)

		if self.cover:
			res_name = ('cover' if (self.cover.id is None) else self.cover.id)
			bi = BookItem(res_name, res_name, self.cover.data, self.cover.mimetype, None)
			opf.manifest.items.append(bi)
			opf.metadata.set('cover', bi.id)
			files.append((('OEBPS/' + res_name), bi))

		for res_num, r in enumerate(r for r in self.references if (r != self.cover)):
			res_name = (('r' + str(res_num)) if (r.id is None) else r.id)
			opf.manifest.items.append(BookItem(res_name, res_name, r.data, r.mimetype, None))
			files.append((('OEBPS/' + res_name), r))

		if self.title:
			opf.metadata.set('title', self.title)
		if self.author:
			opf.metadata.set('creator', self.author)
		if self.source:
			opf.metadata.set('source', self.source)
		elif (self.scheme_uid == 'URI') and self.uid:
			opf.metadata.set('source', self.uid)
		if self.description:
			opf.metadata.set('description', self.description)
		opf.metadata.set('date', ((datetime.datetime.utcnow().isoformat() + '+00:00') if (self.date is None) else self.date))

		with zipfile.ZipFile(file_out, 'w', compression=zipfile.ZIP_DEFLATED) as z_epub:
			z_epub.writestr(self.PATH_MIMETYPE, self.mimetype, compress_type=zipfile.ZIP_STORED)
			z_epub.writestr(self.PATH_CONTAINER, etree.tostring(Container('OEBPS/content.opf').to_xml(), encoding='utf-8', xml_declaration=True))
			z_epub.writestr('OEBPS/content.opf', etree.tostring(opf.to_xml(self.uid, self.scheme_uid), encoding='utf-8', xml_declaration=True))
			for name_file, bi in files:
				z_epub.writestr(name_file, bi.data.get_data())
		return opf

	def __enter__(self): return self
	def __exit__(self, *e):
		pass

class LazyReader(object):
	@property
	def content(self): return self._content.read(self.path)
class ChapterLazy(LazyReader, Chapter):
	pass
class ResourceLazy(LazyReader, Resource):
	pass
class BookFileBacked(Book):
	Chapter = ChapterLazy
	Reference = ResourceLazy
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._handles = []
	def load(self, file_in):
		z_epub = self._open_epub(file_in)
		self._handles = [z_epub, file_in]
		self.chapters.clear()
		self.references.clear()
		self._load(z_epub)
	def _get_content(self, z_epub, path): return z_epub
	def __enter__(self): return self
	def __exit__(self, *e):
		for h in self._handles:
			with h:
				pass
		self._handles.clear()


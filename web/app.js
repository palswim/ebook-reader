"use strict";
(function () {
	if (!Array.prototype.forEach)
		Array.prototype.forEach = function(callback, that) {
			var a = this, len = this.length;
			for (var i = 0; i < len; i++) {
				callback.call(that, a[i], i, a);
			}
		}

	var hasClass = function(el, className) { return ((" " + el.className + " ").indexOf(" " + className + " ") >= 0); }
	var addClass = function(el, className) {
		if (!hasClass(el, className))
			el.className += " " + className;
		return el;
	}
	var removeClass = function(el, className) {
		if (!el.className)
			return el;
		var classes = el.className.split(" ");
		for (var i = classes.length; i--;) {
			if (classes[i] == className) {
				el.className = classes.slice(0, i).concat(classes.slice(i + 1)).join(" ").trim();
				break;
			}
		}
		return el;
	}
	var setSingleClass = function(elSingle, classname, elParent) {
		elParent = elParent || elSingle.parentNode;
		var els = (elParent.childNodes || elParent); // If "elParent" is not an Element, assume an Array
		[].forEach.call(els, function(el) { removeClass(el, classname); }); // el.childNodes is not a true Array
		return addClass(elSingle, classname);
	}
	var blockEvent = function(e) { if (e.preventDefault) e.preventDefault(); if (e.stopPropagation) e.stopPropagation(); return false; }

	var replaceText = function(el, text) {
		var nText = document.createTextNode((typeof text === "undefined") ? "" : text);
		if (el.firstChild)
			el.replaceChild(nText, el.firstChild);
		else
			el.appendChild(nText);
		return el;
	}
	var replaceTextSelector = function(elParent, selector, text) { return replaceText(elParent.querySelector(selector), text); }
	var ensureVisibleInParent = function(el) {
		var rEl = el.getBoundingClientRect();
		for (var elParent = el; elParent = elParent.parentNode;) {
			if (!elParent.getBoundingClientRect)
				break;
			var rParent = elParent.getBoundingClientRect();
			if (rEl.bottom > rParent.bottom)
				elParent.scrollTop += (rEl.bottom - rParent.bottom);
			else if (rEl.top < rParent.top)
				elParent.scrollTop -= (rParent.top - rEl.top);
			rEl = el.getBoundingClientRect();
		}
	}

	var updateChaptersFromSelection = function(elBooks) {
		if (elBooks.value)
			cx.listBook(elBooks.value, function (text) { cx.updateChapters(elBooks.value, text); });
	}
	var mapChapterToItem = function(entry, bid, idx, createElement, elParent) {
		if (typeof entry === "string")
			entry = {title: entry};
		else if (!entry)
			entry = {};
		var elChapter = createElement(idx);
		var children = (entry.c || []);
		for (var i = 0; i < children.length; i++)
			mapChapterToItem(children[i], bid, (idx + "/" + (i + 1)), createElement, elChapter);
		var title = (entry.title || ("Chapter " + idx));
		elChapter.title = title;
		var elLink = replaceTextSelector(elChapter, "a", title);
		elLink.href = urlForChapter(bid, idx);
		if (entry.frag)
			elLink.href += ("#" + entry.frag);
		attachEvents(elChapter, [[elChapter, "click", function () {
			cx.getChapter(bid, idx, function (text) {
				cx.updateText(text);
				if (entry.frag) {
					var elBox = document.querySelector("div.textBox");
					var elTop = elBox.querySelector("#" + entry.frag);
					if (elTop == null) {
						elTop = elBox.querySelector("*[name='" + entry.frag + "']");
						if (elTop == null)
							return;
					}
					textBoxPosBottom = null;
					elBox.scrollTop = elTop.getBoundingClientRect().top;
				}
			});
			document.querySelector(".areaNav form .tracker").value = idx;
			bookmarks[bid] = idx;
			setSingleClass(this, "selected");
			ensureVisibleInParent(this);
		}]]);
		return elParent.appendChild(elChapter);
	}
	var urlForChapter = function (bid, cid) { return ("r/" + bid + "/" + cid); }

	var fillBookDetails = function(elDetails, data) {
		elDetails.querySelectorAll(".data").forEach(function (elItem) {
			var val = "";
			var classNames = elItem.className.split(" ");
			for (var j = classNames.length; j--;) {
				if (classNames[j] == "data")
					continue;
				val = data[classNames[j]] || val;
			}
			switch (elItem.tagName) {
			case "IMG":
				if (val)
					elItem.src = val;
				else
					elItem.removeAttribute("src");
				break;
			case "A":
				if (val)
					elItem.href = val;
				else
					elItem.removeAttribute("href");
				// Fall-through
			default: replaceText(elItem, val); break;
			}
		});
	}

	var focusText = function () {
		var el = document.querySelector("body > .textBox");
		el.focus();
	}
	var markScrollLast = function () {
		var rect = this.getBoundingClientRect();
		var bottomNew = rect.bottom + this.scrollTop;
		if (textBoxPosBottom != null) {
			var elMarker = document.querySelector("div.textMarker");
			if (elMarker != null) {
				var topOld = elMarker.getBoundingClientRect().top;
				elMarker.style.top = ((topOld + textBoxPosBottom - bottomNew) + "px");
			} else if (bottomNew > textBoxPosBottom) {
				elMarker = document.createElement("DIV");
				var elBox = this;
				elBox.appendChild(elMarker);
				elMarker.style.top = ((textBoxPosBottom - this.scrollTop) + "px");
				elMarker.className = "textMarker";
				var duration = getComputedStyle(elMarker)["transition-duration"];
				if ((duration.length > 1) && (duration[duration.length - 1] == "s")) {
					var scale = ((duration[duration - 2] == "m") ? 1 : 1000);
					duration = parseFloat(duration.slice(0, (duration.length - ((scale > 1) ? 1 : 2))));
					if (!isNaN(duration))
						setTimeout(function () { if (elMarker.parentNode == elBox) elBox.removeChild(elMarker); }
							, (scale * duration * 1.05));
				}
			}
		}
		textBoxPosBottom = bottomNew;
		return true;
	}

	var title_orig = document.title;
	var cx = null;
	var initializeConnection = function() {
		var request = function(method, urlRel, body, xhrMod, processSuccess, processFail) {
			addClass(document.body, "r_pending");
			removeClass(document.body, "r_failed");
			var xhr = new XMLHttpRequest();
			xhr.open(method, urlRel);
			if (xhrMod)
				xhrMod(xhr);
			xhr.onreadystatechange = function () {
				if (xhr.readyState == XMLHttpRequest.DONE) {
					removeClass(document.body, "r_pending");
					if ((xhr.status >= 200) && (xhr.status < 400) && processSuccess) {
						processSuccess(xhr.responseText);
					} else if ((xhr.status > 400) && processFail) {
						addClass(document.body, "r_failed");
						processFail(xhr.status, xhr.responseText);
					}
				}
			};
			xhr.onerror = function () {
				removeClass(document.body, "r_pending");
				addClass(document.body, "r_failed");
			};
			xhr.send(body);
		}
		var query = function(url, processSuccess, processFail) {
			return request("GET", url, null, null, processSuccess, processFail);
		}
		var c = {
			list: function(processText) {
				query("l", processText);
			},
			listBook: function(bid, processText) {
				query(("l/" + bid), processText);
			},
			updateBooks: function(text) {
				var items = JSON.parse(text);
				var elBooks = document.querySelector("select.navBooks");
				var elSelect = null;
				var valPrev = elBooks.value;
				while (elBooks.firstChild)
					elBooks.removeChild(elBooks.firstChild);
				items.sort(function (i1, i2) {
					return ((i1.title > i2.title) ? 1 : ((i1.title < i2.title) ? -1 : 0));
				});
				items.forEach(function (item) {
					var elOpt = document.createElement("OPTION");
					elOpt.value = item.id;
					elOpt.appendChild(document.createTextNode(item.title || item.id));
					if ((item == valPrev) || (item.selected && (elSelect == null)))
						elSelect = elOpt;
					elBooks.appendChild(elOpt);
				});
				if (elSelect != null)
					elSelect.selected = true;
				updateChaptersFromSelection(elBooks);
			},
			updateChapters: function(bid, text) {
				var items = JSON.parse(text);
				var elTemplate = document.querySelector("ol.navChapters > li.template");
				while (elTemplate.nextSibling)
					elTemplate.parentNode.removeChild(elTemplate.nextSibling);
				var elSelect = null;
				var idxSelect = bookmarks[bid];
				for (var i = 0; i < items.length; i++) {
					var elNew = mapChapterToItem(items[i], bid, (i + 1), function (idx) {
						var el = removeClass(elTemplate.cloneNode(true), "template");
						if (idx == idxSelect)
							elSelect = el;
						return el;
					}, elTemplate.parentNode);
					if (items[i] && items[i].selected && (elSelect == null))
						elSelect = elNew;
				}
				var elClick = (elSelect || elTemplate.nextSibling);
				if (elClick)
					elClick.click();
			},
			getChapter: function(bid, cid, processText) {
				query(urlForChapter(bid, cid), processText);
			},
			updateText: function(text) {
				var elBox = document.querySelector("div.textBox");
				while (elBox.firstChild)
					elBox.removeChild(elBox.firstChild);
				var elNew = document.createElement("DIV");
				elNew.innerHTML = text;

				if (options.strip.style) {
					var s = [elNew.firstChild];
					while (s.length) {
						var el = s.pop();
						if (el.nextSibling)
							s.push(el.nextSibling);
						if (el.firstChild)
							s.push(el.firstChild);
						if (el.style)
							el.removeAttribute("style");
					}
				}
				elBox.appendChild(elNew);
				attachEvents(elNew, [["a", "click", function () {
					var els = document.querySelectorAll(".areaNav .navChapters a");
					for (var i = 0; i < els.length; i++) {
						if (els[i].href == this.href) {
							els[i].click();
							return false;
						}
					}
					return true;
				}]]);
				elBox.scrollTop = 0;
				focusText();
				textBoxPosBottom = null;
				markScrollLast.apply(elBox);
			},
			bookInfo: function(bid, processText) { return query(("i/" + bid), processText); },
			delete: function(bid, processText) { return query(("del/" + bid), processText); },
			add: function(data) {
				var optSelected = null;
				var elBooks = document.querySelector("select.navBooks");
				for (var i = data.files.length; i--;)
					request("POST", "add", data.files[i], function(xhr) {
						xhr.setRequestHeader("Content-Type", "application/epub+zip");
					}, function (text) {
						var r = JSON.parse(text);
						var elOpt = document.createElement("OPTION");
						if (typeof r === "string")
							r = {id: r};
						elOpt.value = r.id;
						elOpt.appendChild(document.createTextNode(r.title || r.id));
						elBooks.appendChild(elOpt);
						if (!optSelected) {
							optSelected = elOpt;
							elOpt.selected = true;
							updateChaptersFromSelection(elBooks);
						}
					});
			}
		};
		c.list(c.updateBooks);
		return (cx = c);
	}

	var cursorInactivityCheck = (function (cssClass, secs) {
		var timeout = null;
		var handleInactive = function () { addClass(document.body, cssClass); }
		timeout = setTimeout(handleInactive, (secs * 1000));
		return function () {
			clearTimeout(timeout);
			removeClass(document.body, cssClass);
			timeout = setTimeout(handleInactive, (secs * 1000));
		}
	})("mouseHide", 2);
	var deleteCurrentBook = function () {
		actBook(function (elOpt) {
			if (confirm("Please confirm deletion of book: " + (elOpt.firstChild ? (elOpt.firstChild.nodeValue || elOpt.value) : elOpt.value)))
				cx.delete(elOpt.value, cx.updateBooks);
		}, 0);
	}

	var keyCodeMap = {10: "Return", 13: "Enter", 32: " ", 27: "Escape"
		, 38: "ArrowUp", 40: "ArrowDown", 37: "ArrowLeft", 39: "ArrowRight", 36: "Home", 35: "End", 33: "PageUp", 34: "PageDown"
		, 113: "F2", 114: "F3", 115: "F4" };
	var keyHandler = function (e) {
		if (document.activeElement
				&& (((document.activeElement.tagName == "INPUT") && (document.activeElement.type == "text"))
					|| (document.activeElement.tagName == "TEXTAREA")))
			return true; // Allow normal keyboard operations in Input Elements
		if (e.metaKey)
			return true; // Nothing for now

		var key = e.key || keyCodeMap[e.keyCode];
		var sel = function (jump, forBook) {
			if (forBook)
				actBook(function (el) {
					if (!el.selected) {
						el.selected = true;
						updateChaptersFromSelection(el.parentNode);
					}
				}, jump);
			else
				actChapter(function (el) {
					if (!hasClass(el, "selected"))
						el.click();
				}, jump);
		}
		switch (key) {
		case "ArrowUp":    if (e.altKey) sel(-1, true); else return true; break;
		case "ArrowDown":  if (e.altKey) sel( 1, true); else return true; break;
		case "ArrowLeft":  sel(e.ctrlKey ? -Infinity : -1); break;
		case "ArrowRight": sel(e.ctrlKey ?  Infinity :  1); break;
		case "Delete":     deleteCurrentBook(); break;
		case " ":          if (e.ctrlKey) document.querySelector("select.navBooks").focus(); return true;
		case "Escape":     elOverlay.querySelector(".close").click(); break;
		case "i":          document.querySelector(".areaNav .infoBook").click(); break;
		case "g":
			var inputChap = prompt("Go to Chapter:");
			var numChap = parseInt(inputChap);
			if (!isNaN(numChap)) {
				var chaps = document.querySelectorAll(".navChapters > li");
				chaps[Math.max(1, Math.min((chaps.length - 1), numChap))].click();
			}
			break;
		default: return true;
		}
		return false;
	}
	var actChapter = function (act, jump) { actRelative(".navChapters > li", matcherActiveOrClass("selected"), act, jump, 1); }
	var actBook = function (act, jump) { actRelative(".areaNav .navBooks > option", matchSelectOption, act, jump); }
	var matcherActiveOrClass = function (cssClass) {
		return function (els) {
			var pos = null;
			for (var i = els.length; i--;) {
				if (document.activeElement === els[i]) {
					pos = i;
					break; // Focus wins over Selected
				} else if (hasClass(els[i], cssClass)) {
					pos = i;
				}
			}
			return pos;
		}
	}
	var matchSelectOption = function (opts) {
		for (var i = opts.length; i--;) {
			if (opts[i].selected)
				return i;
		}
		return null;
	}
	var actRelative = function (els, findMatch, actOnItem, jump, posMin) {
		if (typeof els === "string")
			els = document.querySelectorAll(els);
		posMin = posMin || 0;
		if (els.length <= posMin)
			return;
		var posNew = (findMatch(els) || 0) + jump;
		var elSelect = els[(posNew < posMin) ? posMin : ((posNew < els.length) ? posNew : (els.length - 1))];
		actOnItem(elSelect);
		removeClass(elOverlay, "active");
	}

	var elOverlay = document.querySelector("body > .overlay");

	var matchesAny = function () {
		for (var i = arguments.length; (--i);)
			if (arguments[i] === arguments[0])
				return true;
		return false;
	}
	var attachEvent = function (o, event, fx) {
		if (o.addEventListener)
			o.addEventListener(event, fx);
		else if (o.attachEvent)
			o.attachEvent(("on" + event), (function () { fx.apply(o, arguments); }));
		else
			o["on" + event] = (function () { fx.apply(o, arguments); });
	};
	var attachEvents = function (elRoot, events) {
		events.forEach(function(args) {
			(function (selector, event, fx) {
				var targets = ((typeof selector === "string") ? elRoot.querySelectorAll(selector) : [selector]);
				var handler = function (e) { return fx.call(this, e) || blockEvent(e); }
				for (var j = targets.length; j--;) {
					attachEvent(targets[j], event, handler);
					if (event == "click")
						attachEvent(targets[j], "keypress", function (e) {
							return (matchesAny((e.key || keyCodeMap[e.key]), "\n", "\r", "Enter", "Return", " ")
								? handler.call(this, e) : true);
						});
				}
			}).apply(null, args);
		});
	}

	attachEvents(document, [
		[".areaNav .navBooks", "change", function () { updateChaptersFromSelection(this); }],
		[".areaNav", "click", function () { if (document.activeElement == document.body) focusText(); }],
		[".areaNav .infoBook", "click", function () {
			fillBookDetails(elOverlay, {});
			addClass(elOverlay, "active");
			cx.bookInfo(this.parentNode.querySelector(".navBooks").value
				, function (json) { fillBookDetails(elOverlay, JSON.parse(json)); });
			elOverlay.querySelector(".content").focus();
			return false;
		}],
		["body > .overlay > .close", "click", function () { removeClass(elOverlay, "active"); focusText(); }],
		["div.textBox", "scroll", markScrollLast],
		[document, "paste", function (e) { return cx.add(e.clipboardData); }],
		[document, "drop" , function (e) { return cx.add(e.dataTransfer) ; }],
		[document, "dragover", function (e) { e.preventDefault(); }],
		[window, "mousemove", cursorInactivityCheck],
		[window, "load", initializeConnection],
		[window, "keydown", keyHandler]
	]);

	var bookmarks = {};
	var options = { strip: { } };
	var textBoxPosBottom = null;
})(); // No global variables


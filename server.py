import sys
import os
import errno
import json
import argparse
import http.server
import urllib.parse
import logging
from book.format.epub import BookFileBacked as EPub
from bs4 import BeautifulSoup

class HTTPException(Exception):
	def __init__(self, code, s, *args, **kwargs):
		if isinstance(code, int):
			self.code = code
		else:
			logging.error('Invalid HTTP Code in error: {}', code)
			self.code = 500
			s = ('Invalid HTTP Code: {}\n' + s)
			args = (code, *args)
		super().__init__(s.format(*args, **kwargs))

class ContentReader(object):
	def __init__(self, data):
		self.data = data.content
		self.mimetype = data.mimetype
	@property
	def headers(self):
		return (() if (self.mimetype is None) else (('Content-Type', self.mimetype),))
	def write_to(self, f_dest):
		f_dest.write(self.data)
class HTMLReader(ContentReader):
	def __init__(self, data, path_base, adjust_link):
		self.data = data.content
		self.path_base = path_base
		self.path = data.path
		self.adjust_link = adjust_link
	@property
	def headers(self):
		return (('Content-Type', 'text/html; charset=utf-8'),)
	def write_to(self, f_dest):
		soup = BeautifulSoup(self.data.decode('utf-8'), features='html.parser')
		for el in soup.findAll('script'):
			el.decompose()
		def tag_touchup(tag, attr, del_rooted, transform_url):
			for el in soup.findAll(tag):
				try:
					val = el.attrs[attr]
				except KeyError:
					continue
				url = urllib.parse.urlparse(val)
				if url.scheme or url.netloc:
					if del_rooted:
						del el.attrs[attr]
				else:
					el.attrs[attr] = transform_url(urllib.parse.urljoin(self.path, val))
		def adjust_url(url): return (self.path_base + '/' + url)
		def adjust_link(url):
			url_rel = self.adjust_link(url)
			return (adjust_url(url) if (url_rel is None) else url_rel)
		for tag in ('img', 'video'):
			tag_touchup(tag, 'src', True, adjust_url)
		tag_touchup('a', 'href', False, adjust_link)
		els = (([soup] if (soup.body is None) else soup.body.children) if (soup.html is None)
			else ([soup] if (soup.html.body is None) else soup.html.body.children))
		for el in els:
			f_dest.write(el.encode('utf-8'))

class EBook(EPub):
	chapter_map = None
def open_book(req, path, force_load = False):
	book = req.server.books.get(path, None)
	if book is None:
		book = EBook()
		req.server.books[path] = book
	elif not force_load:
		return book

	try:
		book.load(open(os.path.join(req.server.dir_base, path), 'rb'))
	except OSError as ex:
		with book:
			pass
		if ex.errno == errno.ENOENT:
			raise HTTPException(404, 'No file at path: {}', os.path.join(req.server.dir_base, path))
		raise
	return book

class BookRoute(object):
	@classmethod
	def dispatch(cls, req, path, *paths):
		with open_book(req, path, True) as book:
			try:
				idx = ((int(paths[0]) - 1) if (len(paths) > 0) else 0)
			except ValueError:
				raise HTTPException(404, 'No Book Item at path: {}', req.path)
			if idx > 0:
				req.server.bookmarks.set(req.sid, path, idx)
			else:
				req.server.bookmarks.drop(req.sid, path)
			if book.chapter_map is None:
				book.chapter_map = {}
				for i, c in enumerate(book.chapters):
					book.chapter_map[c.href] = i
					url_base, _ = urllib.parse.urldefrag(c.href)
					if (url_base != c.href) and (url_base not in book.chapter_map):
						book.chapter_map[url_base] = i
			def replace_chapter_ref(url):
				idx_chap = book.chapter_map.get(url, None)
				if idx_chap is None:
					return None
				chap = book.chapters[idx_chap]
				return ('/'.join((cls.PREFIX_CHAPTER, path, str(idx_chap + 1)))
					+ (('#' + chap.fragment) if chap.fragment else ''))
			return HTMLReader(book.chapters[idx], (cls.PREFIX_STATIC + '/' + path), replace_chapter_ref)
	PREFIX_CHAPTER = 'r'
	PREFIX_STATIC = 's'
	@classmethod
	def dispatch_static(cls, req, path, *paths):
		with open_book(req, path, True) as book:
			path_res = '/'.join(paths)
			try:
				return ContentReader(next(r for r in book.references if (r.path == path_res)))
			except StopIteration:
				raise HTTPException(404, 'No Resource at path: {}', path_res)

	ROUTE_ADD = 'add'
	@classmethod
	def add(cls, req):
		book = EPub()
		data = req.readall()
		import io
		try:
			with io.BytesIO(data) as f:
				book.load(f)
		except ValueError as ex:
			raise HTTPException(400, 'Invalid data in file: {}', ex) from None
		book_id = 0
		while os.path.exists(os.path.join(req.server.dir_base, str(book_id))):
			book_id += 1
		with open(os.path.join(req.server.dir_base, str(book_id)), 'wb') as f:
			f.write(data)
		return {'id': book_id, 'title': book.title}

	@classmethod
	def delete(cls, req, path):
		try:
			del req.server.books[path]
		except KeyError:
			pass
		os.remove(os.path.join(req.server.dir_base, path))
		req.server.bookmarks.drop_bid(path)
		return ListRoute.dispatch(req)

	@classmethod
	def get_info(cls, req, path):
		with open_book(req, path) as book:
			m = {
				'title': book.title,
				'author': book.author,
				'summary': book.description,
				'date': book.date,
				'uid': book.uid,
				'scheme-uid': book.scheme_uid,
			}
			if book.cover is not None:
				m['cover'] = '/'.join((cls.PREFIX_STATIC, path, book.cover.path))
			return m

class ListRoute(object):
	@classmethod
	def dispatch(cls, req, *paths):
		if len(paths) == 0:
			books = [(name, open_book(req, name)) for name in os.listdir(req.server.dir_base)]
			selected = req.server.bookmarks.get_bid(req.sid)
			def book_to_map(bid, book):
				m = {'id': bid, 'title': book.title}
				if bid == selected:
					m['selected'] = 1
				return m
			try:
				return [book_to_map(name, book) for name, book in books]
			finally:
				for name, book in books:
					with book:
						pass
		elif len(paths) > 1:
			raise HTTPException(404, '{}', req.path)
		def chapter_to_map(c, idx):
			m = {'title': c.title}
			items = [chapter_to_map(sub, (str(idx) + '.' + str(i))) for i, sub in enumerate(c.sub)]
			if items:
				m['items'] = items
			if idx == req.server.bookmarks.get(req.sid, paths[0]):
				m['selected'] = 1
			if c.fragment:
				m['frag'] = c.fragment
			return m
		req.server.bookmarks.set(req.sid, paths[0], req.server.bookmarks.get(req.sid, paths[0]))
		with open_book(req, paths[0]) as book:
			return [chapter_to_map(c, i) for i, c in enumerate(book.chapters)]

class RequestHandler(http.server.BaseHTTPRequestHandler):
	sid = '' # Placeholder for multi-session support
	def __len__(self):
		return int(self.headers.get('Content-Length', 0))
	def readall(self):
		return self.rfile.read(len(self))
	def path_parts(self):
		parts = self.decode_url_value(self.path).split('/')
		if len(parts) < 2:
			raise HTTPException(404, 'Invalid path: {}', self.path)
		return parts[1:]

	@staticmethod
	def decode_url_value(v):
		return urllib.parse.parse_qs('k=' + v)['k'][0]

	def dispatch_static(self, *paths):
		if (len(paths) == 1):
			if paths[0] == '':
				return StaticFileContent(self.server.dir_static, 'app.htm')
			elif paths[0].startswith('app.'):
				return StaticFileContent(self.server.dir_static, paths[0])
		raise HTTPException(404, '')
	routes = {
		'GET': {
			BookRoute.PREFIX_CHAPTER: BookRoute.dispatch,
			'read': BookRoute.dispatch,
			BookRoute.PREFIX_STATIC: BookRoute.dispatch_static,
			'static': BookRoute.dispatch_static,
			'res': BookRoute.dispatch_static,
			'del': BookRoute.delete,
			'delete': BookRoute.delete,
			'i': BookRoute.get_info,
			'info': BookRoute.get_info,
			'l': ListRoute.dispatch,
			'list': ListRoute.dispatch,
			None: (lambda self, *p: self.dispatch_static(*p))
		},
		'POST': {
			'add': BookRoute.add
		}
	}
	routes_obj = {
	}
	def dispatch(self):
		try:
			paths = self.path_parts()
			handler = self.routes_obj.get(paths[0], None)
			if handler is None:
				method_dispatch = self.routes.get(self.command, None)
				if method_dispatch is None:
					raise HTTPException(405, 'Unknown Method: {}', self.command)
				handler = method_dispatch.get(paths[0], None)
				if handler is None:
					try:
						handler_fallback = self.routes[self.command][None]
						p0 = paths[0]
						handler = (lambda s, *p: handler_fallback(s, p0, *p))
					except KeyError:
						raise HTTPException(404, '')
			data = handler(self, *paths[1:])
			self.send_response(200)
			self.send_header('Access-Control-Allow-Origin', '*') # CORS
			if hasattr(data, 'headers'):
				for k, v in data.headers:
					self.send_header(k, v)
			elif data is not None:
				self.send_header('Content-Type', 'application/json')
			self.end_headers()
			if hasattr(data, 'write_to'):
				data.write_to(self.wfile)
			elif data is not None:
				self.wfile.write(json.dumps(data).encode('utf-8'))
		except HTTPException as ex:
			if ex.code == 404:
				self.log.error('{}: {} ({})'.format(self.command, ex, self.path))
			else:
				self.log.exception('{}: {}'.format(self.command, self.path))
			try:
				self.send_error(ex.code, explain = (self.path if (ex.code == 404) else str(ex)))
			except OSError as ex:
				if ex.errno != errno.EPIPE:
					raise
				self.log.info('\tAnd a Broken Pipe')
		except Exception as ex:
			if isinstance(ex, OSError) and (ex.errno == errno.EPIPE):
				self.log.info('Broken Pipe (re-using page instance from old invocation): ' + self.path)
			else:
				self.log.exception('')
	def do_GET(self):
		self.dispatch()
	def do_POST(self):
		self.dispatch()
	@property
	def log(self): return self.server.log
	def log_message(self, *args, **kwargs):
		if (self.log.level if (self.log.level > 0) else logging.root.level) <= logging.DEBUG:
			super().log_message(*args, **kwargs)

class StaticFileContent(object):
	def __init__(self, *paths):
		self._paths = paths
	@property
	def headers(self):
		ext = self._paths[-1].split('.')[-1]
		if ext == 'js':
			mtype = 'text/javascript'
		elif ext == 'css':
			mtype = 'text/css'
		else:
			mtype = 'text/html'
		return (('Content-Type', mtype),)
	def write_to(self, f_dest):
		with open(os.path.join(*self._paths), 'rb') as f:
			f_dest.write(f.read())
class Server(http.server.HTTPServer):
	allow_reuse_address = True
	def __init__(self, address, dir_base, dir_static, bookmarks):
		self.dir_base = dir_base
		self.dir_static = dir_static
		self.log = logging.getLogger('server')
		self.books = {}
		self.bookmarks = (bookmarks or BookmarkBase())
		super().__init__(address, RequestHandler)

class BookmarkBase(object):
	def drop_sid(self, sid): pass
	def drop_bid(self, bid): pass
	def get_bid(self, sid):  return None
	def get(self, sid, bid): return None
	def set(self, sid, bid, idx): pass
	def drop(self, sid, bid): pass
class BookmarkManager(BookmarkBase):
	def __init__(self, limit = None):
		self._marks = {}
		self._limit = limit
		if limit is not None:
			import time
			self._now = time.time
		else:
			self._now = (lambda: 0)
	def get_bid(self, sid):  return self._marks.get(sid, None)
	def get(self, sid, bid): return self._marks.get((sid, bid), (None,))[0]
	def set(self, sid, bid, idx):
		self._marks[sid] = bid
		if self._limit is not None:
			if self._limit <= 0:
				return
			marks = sorted(((k, v) for k, v in self._marks.items()
					if (isinstance(k, tuple) and (k[0] == sid) and (k[1] != bid)))
				, key = (lambda k: k[1][1]))
			if len(marks) >= self._limit:
				for key, _ in marks[:(len(marks) + 1 - self._limit)]:
					self._drop_key(key)
		self._marks[(sid, bid)] = (idx, self._now())
	def drop(self, sid, bid):
		return self._drop_key((sid, bid))
	def _drop_key(self, key):
		try:
			del self._marks[key]
		except KeyError:
			pass
	def _drop_key_match(self, matcher):
		for k in list(self._marks.keys()):
			if matcher(k):
				self._drop_key(k)
	def _drop_sid_bid(self, matcher):
		return self._drop_key_match(lambda key: (isinstance(key, tuple) and matcher(*key)))
	def drop_sid(self, sid):
		self._drop_key(sid)
		return self._drop_sid_bid(lambda s, b: (s == sid))
	def drop_bid(self, bid):
		return self._drop_sid_bid(lambda s, b: (b == bid))
	def __str__(self):
		return str(dict((k, (v[0] if isinstance(k, tuple) else v)) for k, v in self._marks.items()))

def __main():
	parser = argparse.ArgumentParser(description="""Serve data from registry of e-Book files""")
	parser.add_argument('-d', '--directory', dest='dir_base', metavar='DIR', help="""Serve EPub files from DIR""")
	parser.add_argument('-a', '--address', dest='address', metavar='ADDRESS', help="""Listen for connections at ADDRESS""")
	parser.add_argument('-p', '--port', dest='port', default=4321, metavar='PORT', help="""Listen on localhost PORT (rather than specifying full ADDRESS)""")
	parser.add_argument('-s', '--static-content', dest='dir_static', metavar='DIR', help="""Serve static content from DIR (default to "./web")""")
	parser.add_argument('-b', '--bookmark-count', dest='cache_book_count', type=int, default=0, help="""Keep bookmarks for latest positions in open books""")
	args = parser.parse_args()

	if args.address is None:
		addr = ('localhost', int(args.port))
	else:
		pos = args.address.rfind(':') # Doesn't quite work completely with IPv6 addresses (e.g. "::1")
		addr = ((args.address if (pos < 0) else args.address[:pos])
			, int(args.port if (pos < 0) else (args.address[(pos + 1):] or args.port)))
	logging.basicConfig(level = logging.INFO)
	server = Server(addr
		, ('.' if (args.dir_base is None) else args.dir_base)
		, ('web' if (args.dir_static is None) else args.dir_static)
		, (BookmarkManager(args.cache_book_count) if (args.cache_book_count > 0) else None))
	def __interrupt(*args): raise SystemExit
	import signal
	signal.signal(signal.SIGTERM, __interrupt)
	try:
		server.serve_forever()
	except (KeyboardInterrupt, SystemExit):
		pass

if __name__ == '__main__':
	__main()

